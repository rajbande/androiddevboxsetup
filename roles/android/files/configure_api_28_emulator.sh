#!/bin/bash
am start -n 'com.android.settings/.DevelopmentSettings'

#Disable animation inside Developer Settings
i=0; while [ $(($i)) -le 26 ]; do i=$(($i + 1));
input keyevent 20;   #   KEYCODE_DPAD_DOWN
done;
i=0; while [ $(($i)) -le 2 ]; do i=$(($i + 1));
input keyevent 20;   #   KEYCODE_DPAD_DOWN
input keyevent 66;   #   KEYCODE_ENTER
input keyevent 19;   #   KEYCODE_DPAD_UP
input keyevent 19;   #   KEYCODE_DPAD_UP
input keyevent 66;   #   KEYCODE_ENTER
done;

#settings put secure show_ime_with_hard_keyboard 0
#settings put secure spell_checker_enabled 0

#Disable keyboard popup and keyboard popup not working with above command. Using alternative below
am start -n 'com.android.settings/.Settings$InputMethodAndLanguageSettingsActivity'
input keyevent 20   #   KEYCODE_DPAD_DOWN
input keyevent 66   #   KEYCODE_ENTER
input keyevent 66   #   KEYCODE_ENTER
input keyevent 4    #   KEYCODE_BACK
input keyevent 20   #   KEYCODE_DPAD_DOWN
input keyevent 20   #   KEYCODE_DPAD_DOWN
input keyevent 66   #   KEYCODE_ENTER
input keyevent 66   #   KEYCODE_ENTER
input keyevent 66   #   KEYCODE_ENTER




