#! /usr/bin/python
from ansible.module_utils.basic import *
from ansible.module_utils.pycompat24 import get_exception

NOT_INSTALLED_VERSION = "-1"

def get_version(module, installer):
    haxm_version_check_command = [installer, '-v']
    haxm_version_check_command_output = module.run_command(haxm_version_check_command, check_rc=False)
    exit_code = haxm_version_check_command_output[0]
    if exit_code == 0:
        haxm_version = haxm_version_check_command_output[1].replace('\n','')
    else:
        haxm_version = NOT_INSTALLED_VERSION
    return haxm_version


def validator(module):
    state = module.params.get('state')
    memory_limit = module.params.get('memory_limit', None)

    if state == 'absent' and memory_limit is not None:
        module.fail_json(changed=False, msg="memory_limit can only be defined when state is present")


def main():
    module = AnsibleModule(
        argument_spec=dict(
            installer=dict(required=True, type='str'),
            state=dict(required=False, type='str', default='present', choices=['present', 'absent']),
            memory_limit=dict(required=False, type='int')
        )
    )

    installer = module.params.get('installer')
    state = module.params.get('state')
    memory_limit = module.params.get('memory_limit', None)

    validator(module)

    version = get_version(module, installer)
    haxm_install_command = [installer]

    if state == 'present':
        if version == NOT_INSTALLED_VERSION:
            if memory_limit is not None:
                haxm_install_command.append('-m')
                haxm_install_command.append(str(memory_limit))
            module.run_command(haxm_install_command, check_rc=True)
            module.exit_json(changed=True, msg="HAXM version %s installed successfully" % get_version(module, installer))
        else:
            module.exit_json(changed=False, msg="HAXM version %s already installed" % version)
    else:
        if version == NOT_INSTALLED_VERSION:
            module.exit_json(changed=False, msg="HAXM not present")
        else:
            haxm_install_command.append('-u')
            module.run_command(haxm_install_command, check_rc=True)
            module.exit_json(changed=True, msg="HAXM version %s uninstalled successfully" % version)


if __name__ == '__main__':
    main()