#! /usr/bin/python
from ansible.module_utils.basic import *
from ansible.module_utils.pycompat24 import get_exception


def main():
    module = AnsibleModule(
        argument_spec=dict(
            name=dict(required=True, type='str'),
            package=dict(required=True, type='str'),
            android_home=dict(required=True, type='str'),
            tag=dict(required=True, type='str'),
            device=dict(required=True, type='str'),
            sd_card=dict(required=True, type='str'),
            force=dict(required=False, type='bool', default=False)
        )
    )

    name = module.params.get('name')
    package = module.params.get('package')
    android_home = module.params.get('android_home')
    tag = module.params.get('tag')
    device = module.params.get('device')
    sd_card = module.params.get('sd_card')
    force = module.boolean(module.params.get('force'))

    avdmanager_create_avd_command = [android_home + "/tools/bin/avdmanager", "create","avd"]
    avdmanager_create_avd_command.append('--name')
    avdmanager_create_avd_command.append(name)
    avdmanager_create_avd_command.append('--package')
    avdmanager_create_avd_command.append(package)
    avdmanager_create_avd_command.append('--tag')
    avdmanager_create_avd_command.append(tag)
    avdmanager_create_avd_command.append('--device')
    avdmanager_create_avd_command.append(device)
    avdmanager_create_avd_command.append('--sdcard')
    avdmanager_create_avd_command.append(sd_card)
    if force:   avdmanager_create_avd_command.append('--force')

    avdmanager_create_avd_output = ""
    avdmanager_create_avd_command_string = " ".join(avdmanager_create_avd_command)

    try:
        avdmanager_create_avd_output = subprocess.check_output(avdmanager_create_avd_command)
    except:
        e = get_exception()
        module.fail_json(
            msg="Tried running: %s \n %s. \nOutput: %s" % (
                avdmanager_create_avd_command_string, str(e), avdmanager_create_avd_output))

    return_message = "Executed: %s.Output: %s." % (
        avdmanager_create_avd_command_string, avdmanager_create_avd_output)
    module.exit_json(changed=True, msg="%s installed successfully." % name + return_message)


if __name__ == '__main__':
    main()
