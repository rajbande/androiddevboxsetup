#! /usr/bin/env python
from ansible.module_utils.basic import *
from ansible.module_utils.pycompat24 import get_exception
import os.path


'''
    src needs to be the absolute path of the dmg file with the '.dmg' extension
    mount_path needs trailing slash
    for installer_type pkg or mpkg, target_volume would typically be '/'
    for installer_type pkg or mpkg, become: yes would typically be required
    for installer_type app, become: yes would typically NOT be required
    creates should point the '.app' file that is created as part of installation
'''

def validate(module):
    installer_type = module.params.get('installer_type')
    target_volume = module.params.get('target_volume')

    if installer_type == 'app' and target_volume is not None:
        return False, "target volume should not be defined with installer_type as 'app'"

    if installer_type in ['pkg','mpkg'] and target_volume is None:
        return False, "target volume must be defined if you have installer_type as '%s'" % installer_type

    return True, None


def check_file_exists(file_path):
    return os.path.exists(file_path)


def install_app(module):
    file_path = module.params.get('file_path')
    mount_path = module.params.get('mount_path')
    installer_name = module.params.get('installer_name')
    installer_type = module.params.get('installer_type')
    target_volume = module.params.get('target_volume')
    hdiutil_executable = module.params.get('hdiutil_executable')
    installer_executable = module.params.get('installer_executable')
    attach_dmg_command = [hdiutil_executable, 'attach', file_path ]
    module.run_command(attach_dmg_command, check_rc=True)

    if installer_type == 'app':
        app_file_path = mount_path + installer_name + '.app'
        install_app_command = ['cp', '-R', app_file_path, '/Applications']
        module.run_command(install_app_command, check_rc=True)
    elif installer_type in ['pkg', 'mpkg']:
        pkg_file_path = mount_path + installer_name + "." + installer_type
        install_pkg_command = [installer_executable, '-package', pkg_file_path, '-target', target_volume]
        module.run_command(install_pkg_command, check_rc=True)

    detach_dmg_command = [hdiutil_executable, 'detach', mount_path]
    module.run_command(detach_dmg_command, check_rc=True)


def main():
    module = AnsibleModule(
        argument_spec=dict(
            file_path=dict(required=True, type='str'),
            mount_path=dict(required=True, type='str'),
            installer_type=dict(required=True, type='str', choices=['app','pkg','mpkg']),
            installer_name=dict(required=True, type='str'),
            target_volume=dict(required=False, type='str'),
            creates=dict(required=True, type='str'),
            hdiutil_executable=dict(required=False, type='str', default='/usr/bin/hdiutil'),
            installer_executable=dict(required=False, type='str', default='/usr/sbin/installer')
        ),
        supports_check_mode=True
    )

    validation_successful, validation_error_message = validate(module)

    if not validation_successful:
        module.fail_json(changed=False, msg=validation_error_message)

    installer_name = module.params.get('installer_name')
    creates = module.params.get('creates')

    if check_file_exists(creates):
        module.exit_json(changed=False, msg="No installation was initiated as %s exists on the target machine" % creates)
    else:
        if not module.check_mode:
            install_app(module)
        module.exit_json(changed=True, msg="Installed %s" % installer_name)


if __name__ == '__main__':
    main()
