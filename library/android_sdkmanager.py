#! /usr/bin/python
import os
from ansible.module_utils.basic import *
from ansible.module_utils.pycompat24 import get_exception

def package_exists(package_name, android_home):
    package_path = android_home + "/%s" % package_name.replace(";", "/")
    return os.path.isdir(package_path)


def main():
    module = AnsibleModule(
        argument_spec=dict(
            package_name=dict(required=True, type='str'),
            android_home=dict(required=True, type='str'),
            proxy=dict(required=False, type='str'),
            proxy_host=dict(required=False, type='str'),
            proxy_port=dict(required=False, type='int'),
            https=dict(required=False, type='bool', default=True)
        )
    )

    package_name = module.params.get('package_name')
    android_home = module.params.get('android_home')
    proxy = module.params.get('proxy')
    proxy_host = module.params.get('proxy_host')
    proxy_port = module.params.get('proxy_port')
    https = module.boolean(module.params.get('https'))

    if package_exists(package_name, android_home):
        module.exit_json(changed=False, msg="%s is already installed." % package_name)
    
    sdkmanager_command = "yes | "+ android_home + "/tools/bin/sdkmanager --licenses"

    try:
        sdkmanager_output = subprocess.check_output(sdkmanager_command, shell=True)
    except:
        e = get_exception()
        module.fail_json(
            msg="Tried running: %s \n %s. \nOutput: %s" % (" ".join(sdkmanager_command), str(e), sdkmanager_output))

    print("All licenses accepted")

    sdkmanager_command = [android_home + "/tools/bin/sdkmanager"]

    sdkmanager_command.append("'%s'" % package_name)
    sdkmanager_output = ""
    try:
        sdkmanager_output = subprocess.check_output(" ".join(sdkmanager_command), shell=True)
    except:
        e = get_exception()
        module.fail_json(
            msg="Tried running: %s \n %s. \nOutput: %s" % (" ".join(sdkmanager_command), str(e), sdkmanager_output))
    return_message = "Executed: %s.Output: %s." % (" ".join(sdkmanager_command), sdkmanager_output)
    module.exit_json(changed=True, msg="Finished")




if __name__ == '__main__':
    main()
